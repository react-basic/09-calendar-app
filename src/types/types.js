

export const types = {

    uiOpenModal: '[ui] Open modal',
    uiCloseModal: '[ui] Close modal',

    eventSetActive: '[event] Set Active',
    eventLogout: '[event] Logout event',

    eventAddNew: '[event] Add new',
    eventStartAddNew: '[event] Start add new',
    eventClearActiveEvent: '[event] Clear active event',
    eventUpdated: '[event] Event updated',
    eventDeleted: '[event] Event deleted',
    eventLoaded: '[event] Events loaded',

    authCheckingFinish: '[auth] Finisih cheking login state',
    authStartLogin: '[auth] Start login',
    authLogin: '[auth] login',
    authStartRegister: '[auth] Start register',
    authStartTokenRenew: '[auth] Start token renew',
    authLogout: '[auth] Logout',

}